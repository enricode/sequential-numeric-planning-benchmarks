#Introduction
This repository contains sequential numeric planning benchmarks with global numeric constraints.
This benchmark suite has been used to test the SPRINGROLL planner, which is the SMT planner
described in "Enrico Scala, Miquel Ramirez, Patrik Haslum, Sylvie Thiebaux "Numeric Planning with Disjunctive Global Constraint via SMT" in Proc. ICAPS 2016". The planner is available [here](https://bitbucket.org/enricode/springroll-smt-hybrid-planner). All the domains are written in an extended version of PDDL 2.1 level 2, where also parametric global constraints can be used to constrain *each* state along the plan trajectory. Global constraints involve both regular and derived variables. For any question just ask!

#Domain Description

##Grouping, Gardening, Pushing Stones, and Counters
This set of domains has been initially presented in Frances G., Hector G. Modeling and computation in planning: Better heuristics from more expressive languages in Proc. of ICAPS, 70–78, to stress the numeric reasoning in planning. This suite presents a slightly, yet equivalent reformulation of Grouping, Gardening and Pushing Stones (here called Simple Sokoban) where we encode a situated agent in a grid where movements are represented as numeric transitions in N^2.

##Hydraulic Blocks World and Power Supply Restoration

These two domains feature switched constraints (Ivankovic et al. Optimal planning with global numerical constraints. In Proc. of ICAPS, 145–153), hereby encoded as disjunctive global numeric constraints. The hydraulic blocks world
is an extended version of blocks world. Here numeric constraints ensure that the blocks, lying on a set of pistons, are displaced in a way to preserve equilibrium of forces given by the
water pression and the weight of blocks on each piston. The power supply restoration domain encodes the problem of selecting switching operations
so as to make a faulty power network operative again. Global constraints in this latter domain capture the Kirchhoff's circuit laws in the network.

##Geometric Rover

The Geometric Rover domain is a new domain, inspired to the one presented in the [3rd International Planning Competition](http://www.icaps-conference.org/index.php/Main/Competitions). The rover is required
to travel between two given locations on the map, performing some tasks along the way. The rover can move along
any of the eight compass directions by a certain amount delta r, a fraction of the extent of the map. The rover battery gets
discharged as it moves around, and can be recharged anywhere by a fixed amount, yet this requires the rover to be
stationary. We consider two types of tasks: collecting soil samples from designated areas and taking pictures of specific locations, within a maximum
distance and relative angle to the target. Traversable areas are defined as disjunctions of numeric constraints. 
Goal locations (e.g., to take picture) are represented by conjunctions of linear inequalities.